#define LIGHT_MAX 0.5f
#define LIGHT_MIN 0.35f
#define RADIUS_FIX 3
#define CONV_SIZE 9

__constant sampler_t sampler =
  CLK_NORMALIZED_COORDS_FALSE
| CLK_ADDRESS_CLAMP_TO_EDGE
| CLK_FILTER_NEAREST;

void FixPixel(float4 *pixel);

__kernel void ProcessImage (
	__read_only image2d_t input,
	__write_only image2d_t output){

    const int2 pos = {
        get_global_id(0),
        get_global_id(1)
    };
    int width = get_image_width(input);
    int height = get_image_height(input);

    float4 pixVal = read_imagef(input, sampler, pos);
    if(pos.x + 1 < get_image_width(input) && pos.x - 1 > 0 && pos.y + 1 < get_image_height(input) && pos.y - 1 > 0){
            pixVal += read_imagef(input, sampler, (int2)(pos.x-1, pos.y)) +
                read_imagef(input, sampler, (int2)(pos.x, pos.y-1))+
                read_imagef(input, sampler, (int2)(pos.x-1, pos.y-1))+
                read_imagef(input, sampler, (int2)(pos.x+1, pos.y))+
                read_imagef(input, sampler, (int2)(pos.x, pos.y + 1))+
                read_imagef(input, sampler, (int2)(pos.x+1, pos.y+1))+
                read_imagef(input, sampler, (int2)(pos.x-1, pos.y+1))+
                read_imagef(input, sampler, (int2)(pos.x+1, pos.y-1));
    }

    pixVal /= 9;
    pixVal.r = (pixVal.r + pixVal.g + pixVal.b)/3;
    pixVal.g = (pixVal.r + pixVal.g + pixVal.b)/3;
    pixVal.b = (pixVal.r + pixVal.g + pixVal.b)/3;
    pixVal.a = 1.0f;

    FixPixel(&pixVal);

    write_imagef (output, (int2)(pos.x, pos.y), pixVal);

    if(pixVal.r == 0 && pixVal.g == 0 && pixVal.b == 0) {
        int k = 0;
        for (int i = -RADIUS_FIX; i < RADIUS_FIX; i++) {
            if (pos.x + i < width && pos.x - i > 0 && pos.y + i < height && pos.y - i > 0) {
                write_imagef(output, (int2)(pos.x + i, pos.y), (float4)(0, 0, 0, 1));
                write_imagef(output, (int2)(pos.x, pos.y + i), (float4)(0, 0, 0, 1));
                write_imagef(output, (int2)(pos.x + i, pos.y + i), (float4)(0, 0, 0, 1));

                write_imagef(output, (int2)(pos.x + i, pos.y - i), (float4)(0, 0, 0, 1));
                write_imagef(output, (int2)(pos.x - i, pos.y + i), (float4)(0, 0, 0, 1));
            }
        }
    }
}

void FixPixel(float4 *pixel){
    pixel->a = 1;
    // correct higher value of light
    if(pixel->r > LIGHT_MAX)
        pixel->r = 1.f;
    if(pixel->g > LIGHT_MAX)
        pixel->g = 1.f;
    if(pixel->b > LIGHT_MAX)
        pixel->b = 1.f;
    barrier(CLK_GLOBAL_MEM_FENCE);
    //correct lower value of light
    if(pixel->r < LIGHT_MIN)
        pixel->r = 0.f;
    if(pixel->g < LIGHT_MIN)
        pixel->g = 0.f;
    if(pixel->b < LIGHT_MIN)
        pixel->b = 0.f;
    barrier(CLK_GLOBAL_MEM_FENCE);
}