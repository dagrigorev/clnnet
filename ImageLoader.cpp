//
// Created by Dmitry Grigorev on 22.02.2018.
//

#include "ImageLoader.h"
#include <fstream>
#include <sstream>
#include <vector>

void ImageLoader::LoadImage(const char *path) {
    std::ifstream in (path, std::ios::binary);

    std::string s;
    in >> s;

    if (s != "P6") {
        exit (1);
    }

    // Skip comments
    for (;;) {
        getline (in, s);

        if (s.empty ()) {
            continue;
        }

        if (s [0] != '#') {
            break;
        }
    }

    std::stringstream str (s);
    int width, height, maxColor;
    str >> width >> height;
    in >> maxColor;

    if (maxColor != 255) {
        exit (1);
    }

    {
        // Skip until end of line
        std::string tmp;
        getline(in, tmp);
    }

    std::vector<char> data (width * height * 3);
    in.read (reinterpret_cast<char*> (data.data ()), data.size ());

}

void ImageLoader::SaveImage(const char *path) {
    std::ofstream out (path, std::ios::binary);

    out << "P6\n";
    out << width << " " << height << "\n";
    out << "255\n";
    out.write (pixel.data (), pixel.size ());
}

void ImageLoader::ConvertToRGB() {
    std::vector<char> temp;
    for (std::size_t i = 0; i < pixel.size (); i += 3) {
        temp.push_back (pixel [i + 0]);
        temp.push_back (pixel [i + 1]);
        temp.push_back (pixel [i + 2]);
        temp.push_back (0);
    }

    this->pixel = temp;
}

int ImageLoader::Width() const {
    return width;
}

int ImageLoader::Height() const {
    return height;
}

std::vector<char> &ImageLoader::Col(int pixelCol) {
    // TODO: fill row
    return col;
}

std::vector<char> &ImageLoader::Row(int pixelRow) {
    // TODO: fill col
    return row;
}