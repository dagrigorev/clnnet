//
// Created by Dmitry Grigorev on 22.02.2018.
//

#include "CLRunner.h"
#include <cstdio>
#include <sstream>
#include <fstream>

CLRunner::CLRunner() {
    this->Init();
}

CLRunner::~CLRunner() {
    // TODO: release all fields

    for(int i=0;i<program_kernels.capacity(); ++i)
        clReleaseKernel(program_kernels[i].kernel);
    for(int j=0;j<loaded_kernels.capacity(); ++j)
        clReleaseProgram(loaded_kernels[j].program);
    loaded_kernels.clear();
    program_kernels.clear();

    clReleaseContext (context);
}

void CLRunner::cl_platform_struct::Load() {
    this->platformsCount = 0;
    clGetPlatformIDs(0, nullptr, &this->platformsCount);

    if(this->platformsCount){
        GetPlatforms();
        GetDevices();
    }else{
        printf("Can't find available OpenCL platforms");
        exit(OPENCL_NOT_FOUND);
    }
}

void CLRunner::cl_platform_struct::PrintPlatforms() {
    printf("OpenCL Platforms: \n");
    for (cl_uint i = 0; i < this->platformsCount; ++i)
        printf("\t(%d): %s\n", i, this->GetPlatformName(this->availablePlatforms[i]).c_str());
}

string CLRunner::cl_platform_struct::GetPlatformName(cl_platform_id platform) {
    size_t size = 0;
    clGetPlatformInfo (platform, CL_PLATFORM_NAME, 0, nullptr, &size);

    std::string result;
    result.resize (size);
    clGetPlatformInfo (platform, CL_PLATFORM_NAME, size,
                       const_cast<char*> (result.data ()), nullptr);

    return result;
}

void CLRunner::cl_platform_struct::GetDevices() {
    this->devicesCount = 0;
    clGetDeviceIDs(currentPlatformId,  CL_DEVICE_TYPE_ALL, 0, nullptr, &this->devicesCount);
    if(this->devicesCount){
        clGetDeviceIDs(this->currentPlatformId, CL_DEVICE_TYPE_ALL, this->devicesCount, this->availableDevices.data(), nullptr);
        bool gpu_found = false;
        for(cl_uint i=0;i<this->devicesCount;++i){
            cl_device_type type;
            clGetDeviceInfo(this->availableDevices[i], CL_DEVICE_TYPE, sizeof(cl_device_type), &type, nullptr);
            if(type == CL_DEVICE_TYPE_GPU){
                this->currentDeviceId = this->availableDevices[i];
                gpu_found = true;
                break;
            }
        }
        if(!gpu_found)
            currentDeviceId = this->availableDevices[0];
    }else{
        currentDeviceId = 0;
        printf("Not available OpenCL devices found\n");
        exit(OPENCL_DEVICE_NOT_FOUND);
    }
}

void CLRunner::cl_platform_struct::GetPlatforms() {
    this->availablePlatforms.resize((size_t)this->platformsCount);
    clGetPlatformIDs(this->platformsCount, this->availablePlatforms.data(), nullptr);
    this->currentPlatformId = this->availablePlatforms[0];
}

void CLRunner::cl_platform_struct::PrintDevices() {
    printf("OpenCL Device: \n");
    for (cl_uint i = 0; i < this->devicesCount; ++i)
        printf("\t(%d): %s\n", i, this->GetDeviceName(this->availableDevices[i]).c_str());
}

string CLRunner::cl_platform_struct::GetDeviceName(cl_device_id device) {
    size_t size = 0;
    clGetDeviceInfo (device, CL_DEVICE_NAME, 0, nullptr, &size);

    std::string result;
    result.resize (size);
    clGetDeviceInfo (device, CL_DEVICE_NAME, size,
                       const_cast<char*> (result.data ()), nullptr);

    return result;
}

void CLRunner::CreateContext() {
    const cl_context_properties contextProperties [] =
            {
                    CL_CONTEXT_PLATFORM, reinterpret_cast<cl_context_properties> (this->cl_pfm.currentPlatformId),
                    0, 0
            };

    cl_int error = CL_SUCCESS;
    this->context = clCreateContext (contextProperties, this->cl_pfm.devicesCount,
                                          this->cl_pfm.availableDevices.data(), nullptr, nullptr, &error);
    CheckError(error);

    this->commands = clCreateCommandQueue(context, cl_pfm.currentDeviceId, 0, &error);
    CheckError(error);

}

void CLRunner::CheckError(cl_int error) {
    if(error != CL_SUCCESS) {
        printf("OpenCL call error!\n");
        exit(error);
    }
}

void CLRunner::Init() {
    this->cl_pfm.Load();
    CreateContext();
}

void CLRunner::LoadProgram(const char *kernel_file_name,
                           const char *short_name_prog,
                           int proto_count,
                           const char *proto_list[]) {
    std::ifstream in(kernel_file_name);
    std::string source (
            (std::istreambuf_iterator<char> (in)),
            std::istreambuf_iterator<char> ());

    size_t lengths [1] = { source.size () };
    const char* sources [1] = { source.data () };

    cl_int error = 0;
    cl_program program = clCreateProgramWithSource (context, 1, sources, lengths, &error);
    CheckError (error);

    // TODO: Building cl_program

    loaded_kernels.push_back(name_prog_struct(string(short_name_prog), program));
    for(int i=0;i<proto_count;++i) {
        cl_kernel kernel = clCreateKernel(program, proto_list[i], &error);
        CheckError(error);
        this->program_kernels.push_back(prog_kernel_struct(program, kernel));
    }
}

void CLRunner::RunProgram(const char *kernel_function, const char *kernel_name) {
    string name(kernel_name);
    string func_name(kernel_function);
    char func_name_arg[func_name.length()];

    auto prog = std::find_if(loaded_kernels.begin(), loaded_kernels.end(), [=](const name_prog_struct &item){
        return item.name == name;
    })->program;
    auto kern = std::find_if(program_kernels.begin(), program_kernels.end(), [=](const prog_kernel_struct &item){
        return item.program == prog;
    })->kernel;

    // TODO: Running here clEnqueueNDRangeKernel
}

void CLRunner::AddProto(const char *proto, const char *kernel_name) {
    string name(kernel_name);
    string proto_name(proto);

    auto prog = std::find_if(loaded_kernels.begin(), loaded_kernels.end(), [=](const name_prog_struct &item){
        return item.name == name;
    })->program;

    if(prog){
        cl_int error;
        cl_kernel kernel = clCreateKernel(prog, proto, &error);
        CheckError(error);
        this->program_kernels.push_back(prog_kernel_struct(prog, kernel));
    }
}

CLRunner::name_prog_struct::name_prog_struct(string name, cl_program program) {
    this->program = program;
    this->name = name;
}

CLRunner::prog_kernel_struct::prog_kernel_struct(cl_program program, cl_kernel kernel) {
    this->program = program;
    this->kernel = kernel;
}