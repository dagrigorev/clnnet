//
// Created by Dmitry Grigorev on 22.02.2018.
//

#ifndef CLNNET_CLRUNNER_H
#define CLNNET_CLRUNNER_H

#include <list>
#include <vector>
#include <string>
#include <unordered_set>
#ifdef __APPLE__
#include "OpenCL/opencl.h"
#else
#include "CL/cl.h"
#endif

using namespace std;

class CLRunner {
    //Private data types
private:
    struct cl_platform_struct {
        const int OPENCL_NOT_FOUND = 1;
        const int OPENCL_DEVICE_NOT_FOUND = 2;

        cl_device_id currentDeviceId;
        cl_platform_id currentPlatformId;
        vector<cl_device_id> availableDevices;
        vector<cl_platform_id> availablePlatforms;
        cl_uint platformsCount;
        cl_uint devicesCount;

        void Load();
        void PrintPlatforms();
        void PrintDevices();
        string GetPlatformName(cl_platform_id platform);
        string GetDeviceName(cl_device_id device);
        void GetPlatforms();
        void GetDevices();
    };

    struct name_prog_struct{
        string name;
        cl_program program;
        name_prog_struct(string name, cl_program program);
    };

    struct prog_kernel_struct{
        cl_program  program;
        cl_kernel kernel;

        prog_kernel_struct(cl_program program, cl_kernel kernel);
    };

public:
    CLRunner();
    ~CLRunner();
    void LoadProgram(const char *kernel_file_name, const char *short_name,
                     int proto_count, const char *proto_list[]);
    void RunProgram(const char *kernel_function, const char *kernel_name);
    void AddProto(const char *proto, const char *kernel_name);

    //Private methods
private:
    void Init();
    void CreateContext();
    void CheckError(cl_int error);

    // Members
private:
    CLRunner::cl_platform_struct cl_pfm;
    cl_context context;
    cl_command_queue commands;
    std::vector<CLRunner::name_prog_struct> loaded_kernels;
    std::vector<CLRunner::prog_kernel_struct> program_kernels;
};


#endif //CLNNET_CLRUNNER_H
