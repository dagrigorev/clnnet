//
// Created by Dmitry Grigorev on 22.02.2018.
//

#ifndef CLNNET_CNETWORK_H
#define CLNNET_CNETWORK_H

#include <vector>
#include <cmath>

class CNetwork {
public:
    CNetwork();
private:
    float *input;
    float *weights;
    float *bias;
};


#endif //CLNNET_CNETWORK_H
