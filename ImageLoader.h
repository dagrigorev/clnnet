//
// Created by Dmitry Grigorev on 22.02.2018.
//

#ifndef CLNNET_IMAGELOADER_H
#define CLNNET_IMAGELOADER_H

#include <vector>

class ImageLoader {
public:
    ImageLoader();
    void LoadImage (const char* path);
    void SaveImage (const char* path);
    void ConvertToRGB();
    int Width() const;
    int Height() const;
    std::vector<char> &Row(int pixelRow);
    std::vector<char> &Col(int pixelCol);

private:
    std::vector<char> pixel, row, col;
    int width, height;
};


#endif //CLNNET_IMAGELOADER_H
