__constant sampler_t sampler =
  CLK_NORMALIZED_COORDS_FALSE
| CLK_ADDRESS_CLAMP_TO_EDGE
| CLK_FILTER_NEAREST;

__kernel void Select(__read_only image2d_t input, __write_only image2d_t output){
    int width = get_image_width(input);
    int height = get_image_height(input);
    int2 pos = {get_global_id(0), get_global_id(1)};

    //sum_row += read_imagef(input, sampler, pos).r;

    write_imagef(output, pos, read_imagef(input, sampler, pos));
}